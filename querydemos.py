"""
# ***(1)Returns all customers from customer table
customer = Customer.objects.all()

# ***(2)Returns first customer from customer table
firstcustomer = Customer.objects.first()

# ***(3)Returns last customer from customer table
lastcustomer = Customer.objects.last()

# ***(4)Returns single customer from customer table by name
customerByName = Customer.objects.get(name='ashkan')

# ***(5)Returns single customer from customer table by id
customerById = Customer.objects.get(id=4)

# ***(6)Returns all orders related to customer (firstcustomer variable set above)
firstcustomer.order_set.all()

# ***(7)Returns orders customer name: (query parent model values)
order = order.objects.first()
parentName = order.customer.name

# ***(8)Returns products from products table with value of "Out Door" in category attribute
products = Products.objects.filter(category='Out Door')

# ***(9) order/sort Objects by id
LeastToGreatest = Product.objects.all().order_by('id')
GreatestToLeast = Product.objects.all().order_by('-id')

# ***(10)returns all products with tag of "Sports": (Query many to many fields)
productsFiltered = Product.objects.filter(tags__name="Sports")

'''
***(11) Bonus
Q: if the customer has more than 1 ball, how would you reflect it in the database?
A: Because there are many different products and this value changes contantly you would most
likely not want to store the value in the database but rather just make this function we can run
'''
# returns the total count for number of time a "Ball" was ordered by the first customer
ballOrders = firstcustomer.order_set.filter(producst__name="Ball").count()

# returns total count for each product ordered
allOrders = {}

for order in firstcustomer.order_set.all():
    if order.product.name in allOrders:
        allOrders[order.product.name] += 1
    else:
        allOrders[order.product.name] = 1

# _^_ returns:allOrders : {'Balls':2, 'BBQ Grill':1}
