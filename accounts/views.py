from django.shortcuts import render, redirect
from .models import (
    Product,
    Order,
    Customer,
    Tag,
)
from .forms import OrderForm, CustomerForm, ProductForm, CreateUserForm
from django.forms import inlineformset_factory
from django import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .filters import OrderFilter
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from .decorators import *

from django.db.models.functions import Cast
from django.db.models.fields import DateField, DateTimeField


def registerPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = CreateUserForm()
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, 'Account was created for \"' + user + '\"')
                return redirect('login')

        context = {
            'form': form
        }
        return render(request, 'accounts/register.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Username or Password is incorrect')
        context = {}
        return render(request, 'accounts/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('login')


# @login_required(login_url='login')
# @allowed_users(allowed_roles=['admin'])
def home(request):
    orders = Order.objects.all()
    customers = Customer.objects.all()

    total_customers = customers.count()
    total_orders = orders.count()
    delivered = orders.filter(status='Delivered').count()
    pending = orders.filter(status='Pending').count()
    OutOfDelivery = orders.filter(status='Out of delivery').count()
    TT = Order.objects.all().values('date_created')
    # TT = Order.objects.annotate(date_only=Cast('date_created', output_field=DateTimeField()))
    # TO = Order.objects.all().values('customer__order__date_created')
    TO = Order.objects.values('product__order__customer').order_by('customer__order')

    page = request.GET.get('page', 1)

    paginator = Paginator(orders, 5)
    try:
        pg = paginator.page(page)
    except PageNotAnInteger:
        pg = paginator.page(1)
    except EmptyPage:
        pg = paginator.page(paginator.num_pages)

    context = {
        # 'orders': orders,
        'customers': customers,
        'total_customers': total_customers,
        'total_orders': total_orders,
        'delivered': delivered,
        'pending': pending,
        'OutOfDelivery': OutOfDelivery,
        'pg': pg,
        'TT': TT,
        'TO': TO,


    }
    return render(request, 'accounts/dashboard.html', context)


def product(request):
    products = Product.objects.all()
    context = {
        'products': products
    }
    return render(request, 'accounts/products.html', context)


def customer(request, pk):
    customer = Customer.objects.get(id=pk)

    orders = customer.order_set.all()
    order_count = orders.count()
    myFilter = OrderFilter(request.GET, queryset=orders)
    orders = myFilter.qs

    context = {
        'customer': customer,
        'orders': orders,
        'order_count': order_count,
        'myFilter': myFilter,

    }
    return render(request, 'accounts/customer.html', context)


def CreateOrder(request, pk):
    OrderFormSet = inlineformset_factory(Customer, Order, fields=('product', 'status'), extra=5, widgets={
        'product': forms.Select(attrs={'class': 'form-control'}),
        'status': forms.Select(attrs={'class': 'form-control'}),
    })
    customer = Customer.objects.get(id=pk)
    formset = OrderFormSet(queryset=Order.objects.none(), instance=customer)
    if request.method == 'POST':
        # print('request post:',request.POST)
        formset = OrderFormSet(request.POST, instance=customer)
        if formset.is_valid():
            formset.save()
            return redirect('home')
    context = {
        'formset': formset
    }
    return render(request, 'accounts/order_form.html', context)


'''
def CreateOrder(request, pk):
    customer = Customer.objects.get(id=pk)
    form = OrderForm(initial={'customer':customer})
    if request.method == 'POST':
        form = OrderForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)
'''


def UpdateOrder(request, pk):
    order = Order.objects.get(id=pk)
    form = OrderForm(instance=order)

    if request.method == 'POST':
        # print('request post:',request.POST)
        form = OrderForm(request.POST or None, instance=order)
        if form.is_valid():
            form.save()
            return redirect('/')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)


@login_required(login_url='login')
def DeleteOrder(request, pk):
    order = Order.objects.get(id=pk)
    if request.method == 'POST':
        order.delete()
        return redirect('/')
    context = {
        'item': order
    }
    return render(request, 'accounts/delete.html', context)


def CreateCustomer(request):
    customer = Customer.objects.all()
    form = CustomerForm()
    if request.method == 'POST':
        form = CustomerForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)


def UpdateCustomer(request, pk):
    ctor = Customer.objects.get(id=pk)
    form = CustomerForm(instance=ctor)

    if request.method == 'POST':
        # print('request post:',request.POST)
        form = CustomerForm(request.POST, instance=ctor)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)


# @login_required(login_url='login')
def DeleteCustomer(request, pk):
    ctor = Customer.objects.filter(id=pk)
    
    if request.method == 'POST':
        ctor.delete()
       
        return redirect('/')
    context = {
        'item': ctor
    }
    return render(request, 'accounts/delete.html', context)


def CreateProduct(request):
    product = Product.objects.all()
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('home')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)


def UpdateProduct(request, pk):
    product = Product.objects.get(id=pk)
    form = ProductForm(instance=product)
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=product)
        if form.is_valid():
            form.save()
            return redirect('products')
    context = {
        'form': form
    }
    return render(request, 'accounts/order_form.html', context)


def DeleteProduct(request, pk):
    product = Product.objects.get(id=pk)
    if request.method == 'POST':
        product.delete()
        return redirect('/')
    context = {
        'item': product
    }
    return render(request, 'accounts/delete.html', context)
