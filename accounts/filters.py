import django_filters
from django.forms import DateInput, TextInput, Select

from .models import Order
from django_filters import DateFilter, CharFilter
from django import forms


class OrderFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name='date_created', lookup_expr='gte',
                            widget=DateInput(attrs={'class': 'datetime'}))
    end_date = DateFilter(field_name='date_created', lookup_expr='lte',
                          widget=DateInput(attrs={'class': 'datetime'}))
    note = CharFilter(field_name='note', lookup_expr='icontains',)

    class Meta:
        model = Order
        fields = [
            'product',
            'date_created',
            'status',
            'note'
        ]
        exclude = ['customer', 'date_created']
        widgets = {
            'product': forms.Select(
                attrs={
                    'class': 'form-group'
                }
            ),
            'status': forms.Select(
                attrs={
                    'class': 'form-group'
                }
            ),
        }
