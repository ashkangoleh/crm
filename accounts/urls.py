from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name='home'),
    path("products/", views.product, name="products"),
    path("customer/<str:pk>/", views.customer, name="customer"),

    path('create_order/<str:pk>/', views.CreateOrder, name='create_order'),
    path('update_order/<str:pk>/', views.UpdateOrder, name='update_order'),
    path('delete_order/<str:pk>/', views.DeleteOrder, name='delete_order'),
    path('update_customer/<str:pk>/', views.UpdateCustomer, name='update_customer'),
    path('delete_customer/<str:pk>/', views.DeleteCustomer, name='delete_customer'),
    path('update_product/<str:pk>/', views.UpdateProduct, name='update_product'),
    path('delete_product/<str:pk>/', views.DeleteProduct, name='delete_product'),
    path('create_customer/', views.CreateCustomer, name='create_customer'),
    path('create_product/', views.CreateProduct, name='create_product'),
    path('register/', views.registerPage, name='register'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
]
