from django.contrib import admin

# Register your models here.
from .models import Customer, Order, Product, Tag


@admin.register(Customer)
class CustomerView(admin.ModelAdmin):
    list_display = (
        'name',
        'phone'
    )


@admin.register(Order)
class OrderView(admin.ModelAdmin):
    list_display = (
        'customer',
        'product',
        'date_created',
        'status',
    )


@admin.register(Product)
class ProductView(admin.ModelAdmin):
    list_display = (
        'name',
        'price',
        'category',
        'date_created',
    )


@admin.register(Tag)
class TagView(admin.ModelAdmin):
    list_display = (
        'name',
    )
